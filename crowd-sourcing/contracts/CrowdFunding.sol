// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.7.0 <0.9.0;

contract CrowdFunding {
  //the manager is the owner of the crowdfunding campaign
  address public manager;
  uint256 public minimumContribution;
  //the approvers must vote on requests for approval
  //the approvers mapping is true if the address is on the approvers list and false otherwise
  //approversCount counts how many approvers have joined the contrac
  uint256 public approversCount;
  mapping(address=>bool) public approvers;
  //this mapping maps a requestID to the request
  //this is needed because the Request struct has a nested mapping which cannot be constructed
  uint256 public requestID;
  mapping(uint=>Request) public requests;
  struct Request{
    string description;
    uint value;
    address payable recipient;
    bool complete;
    uint256 approvalCount;
    mapping(address=>bool) votersWhoApproved;
  }

  modifier restricted(){
    require(msg.sender == manager);
    _;
  }

  constructor(uint256 min, address creator){
    manager = creator;
    minimumContribution = min;
    requestID = 0;
  }

  //as soon as anyone contributes, he joins the list of approvers and is granted the power to vote on spending requests
  function contribute() payable public{
    require(msg.value >= minimumContribution, 'The amount sent was less than the minimum contribution.');
    approvers[msg.sender] = true;
    approversCount++;
  }

  //the manager can request funds at any time, but they are only released after a voting from the approvers
  function createRequest(
    string calldata description,
    uint value,
    address payable recipient)
    public restricted
    returns(uint newRequestID){
      //the right hand side is about creating the variable, while the left side is about storing it. Keep that in mind
      //whenever creating a new variable inside of a function, it needs to be declared as storage or memory
      //newRequest doesn't refer to any element in storage. Therefore it is a storage element.
      // Request memory newRequest = Request({
      //   description:description,
      //   value:value,
      //   recipient:recipient,
      //   complete:false,
      //   approvalCount:0
      //   //reference types, like mappings don't need to be initialized
      //   });

      //this new approach is needed because a struct with a nested mapping cannot be constructed
      //instead, remember that when a mapping receives an uninitialized key, it returns an null value or an empty struct
      //a pointer is created to that empty struct in storage and that is used to alter the referenced empty struct
      requestID++;
      Request storage newRequest = requests[requestID];
      newRequest.description = description;
      newRequest.value = value;
      newRequest.recipient = recipient;
      newRequest.complete = false;
      newRequest.approvalCount = 0;
      return requestID;
  }

  function requestApproving(uint index) public{
    //requires that the sender is on the approvers list, and has not voted on this same request yet
    //lookups in mappings are more espensive than reading variables
    require(approvers[msg.sender]);
    Request storage selectedRequest = requests[index];
    require(!selectedRequest.votersWhoApproved[msg.sender]);

    selectedRequest.votersWhoApproved[msg.sender] = true;
    selectedRequest.approvalCount++;
  }

  ///@notice The manager finalizes a request.
  ///@dev Check if the request has already been completed, and if it has enough approving votes.
  ///@dev then send the ammount approved to the manager
  function finalizeRequest(uint index) public restricted{
    Request storage request = requests[index];
    require(!request.complete,'the request has already been completed');
    require(request.approvalCount > (approversCount / 2));
    request.recipient.transfer(request.value);
    request.complete = true;
  }
}
