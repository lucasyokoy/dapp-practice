// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.7.0 <0.9.0;

import './CrowdFunding.sol' as cf;

///@title Campaign factory: creates, registers and manages campaign contracts
///@dev functions: getCampaigns, createCampaign, getOneCampaign
///@dev variables: campaignList (list of addresses of the contracts), manager
///@dev all function calls are implemented without side effects
contract CampaignFactory{
    address[] public campaignList;
    address public manager;
    uint numberOfCampaigns;
    constructor() {
        manager = msg.sender;
        numberOfCampaigns = 0;
    }

    ///@notice creates a new campaign and appends it to the campaignList
    ///@dev when the new contract is created, it's instance is returned by the constructor
    ///@dev when creating the contract, if msg.sender is set as the manager, the CampaignFactory contract will be set as the manager, when it calls the new campaign function. To solve that, we pass the creator's address as an argument
    ///@dev when calling the new contract creator, don't forget to call the contract inside the file, not just the name of the file
    function createCampaign(uint minimumContribution) public returns(uint){
        cf.CrowdFunding newCampaign = new cf.CrowdFunding(minimumContribution, msg.sender);
        campaignList.push(address(newCampaign));
        numberOfCampaigns ++;
        return numberOfCampaigns;
    }

    ///@notice returns the whole list of campaigns
    function getCampaigns() public view returns(address[] memory){
        return campaignList;
    }

    ///@notice accesses a specified campaign
    function getOneCampaign(uint campaignIndex) public view returns(cf.CrowdFunding){
        return cf.CrowdFunding(campaignList[campaignIndex]);
    }
}